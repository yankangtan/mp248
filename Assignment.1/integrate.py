def sym_gauss_int_sqr(xb):
    n = 10
    width = 2*xb/n
    x_coors = linspace(-xb,xb-width,n)
    area = 0
    for x_coor in x_coors:
        height = 0.5 * (exp(-(x_coor**2)) + exp(-((x_coor+width)**2)))
        area = area + width*height
    return area